import React, { Component } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import AppFrame from '../components/AppFrame';
import { getPostById } from '../selectors/posts';
import { Route, withRouter } from 'react-router-dom';
import PostEdit from '../components/PostEdit';
import PostData from '../components/PostData';
import { fetchPosts } from '../actions/fetchPosts';
import { updatePost } from '../actions/updatePost';
import { SubmissionError } from 'redux-form';
import { deletePost } from '../actions/deletePost';

class PostContainer extends Component {

    componentDidMount() {
        if (!this.props.post) {
            console.log(`componentDidMount de postContainer ${this.props}` );
            this.props.fetchPosts();
        }
    }

    handleSubmit = values => {
        // console.log(JSON.stringify(values));
        const { id } = values; 
        return this.props.updatePost(id, values).then( r => {
            if (r.error) {
                console.log(r.error);
                throw new SubmissionError(r.payload);
            }
        });
    }

    handleOnBack = () => {
        this.props.history.goBack();
    }

    handleOnSubmitSuccess = () => {
        this.props.history.goBack();
    }

    handleOnDelete = id => {
        console.log("handleOnDelete");
        this.props.deletePost(id).then(v => {
            this.props.history.goBack();
        });
    }

    renderPostControl = (isEdit, isDelete) => {
        if (this.props.post) {
            const PostControl = isEdit ? PostEdit : PostData;
            return <PostControl {...this.props.post} 
                        onSubmit={this.handleSubmit}
                        onSubmitSuccess={this.handleOnSubmitSuccess}
                        onBack={this.handleOnBack}
                        isDeleteAllow={!!isDelete}
                        onDelete={this.handleOnDelete} />
        }

        return null;        
    }

    renderBody = () => (
        <Route path="/posts/:id/edit" children={
            ( { match: isEdit } ) => (
                <Route path="/posts/:id/del" children={
                    ( { match: isDelete } ) => (
                        this.renderPostControl(isEdit, isDelete))
            } /> )
        } />
    )
    render() {
        return (
            <div>
                <AppFrame header={`Post ${this.props.id}`}
                    body={this.renderBody()} >
                </AppFrame>
            </div>
        );
    }
}

PostContainer.propTypes = {
    post: PropTypes.object,
    fetchPosts: PropTypes.func.isRequired,
    updatePost: PropTypes.func.isRequired,
    deletePost: PropTypes.func.isRequired,
};

const mapStateToProps = (state, props) => ({
    post: getPostById(state, props)
});



export default withRouter(connect(mapStateToProps, {
    fetchPosts,
    updatePost,
    deletePost
})(PostContainer));