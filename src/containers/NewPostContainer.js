import React, { Component } from 'react';
import PropTypes from 'prop-types';
import AppFrame from '../components/AppFrame';
import PostEdit from '../components/PostEdit';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import { insertPost } from '../actions/insertPost';
import { SubmissionError } from 'redux-form';

class NewPostContainer extends Component {

    handleSubmit = values => {
        return this.props.insertPost(values).then( r => {
            if (r.error) {
                throw new SubmissionError(r.payload);
            }
        });
    }

    handleOnSubmitSuccess = () => {
        this.props.history.goBack();
    }

    handleOnBack = () => {
        this.props.history.goBack();
    }

    renderBody = () => {
        const newPost = {
            "UserId": 1,
            "id":undefined,
            "title": "",
            "body": "",
            
          };
        return <PostEdit {...newPost} onSubmit={this.handleSubmit}
                    onSubmitSuccess={this.handleOnSubmitSuccess}
                    onBack={this.handleOnBack} />
    }

    render() {
        return (
            <div>
                <AppFrame header={`Create new Post`}
                    body={this.renderBody()}>
                </AppFrame>
            </div>
        );
    }
}

NewPostContainer.propTypes = {
    insertPost: PropTypes.func.isRequired,
};

export default withRouter(connect(null, { insertPost })(NewPostContainer));