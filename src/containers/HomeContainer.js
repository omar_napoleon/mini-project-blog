import React, { Component } from 'react';
import { withRouter } from 'react-router-dom';
import AppFrame from './../components/AppFrame';
import PostsActions from '../components/PostsActions';

class HomeContainer extends Component {

    handleOnClick = () => {
        console.log("handleOn Click");
        this.props.history.push('/posts');
    }

    render() {
        return (
            <div>
                <AppFrame
                    header='Home'
                    body={
                        <div>
                            <PostsActions>
                                <button onClick={this.handleOnClick} >Show list</button>
                                
                            </PostsActions>        
                        </div>
                    }

                    ></AppFrame>
            </div>
        );
    }
}

export default withRouter(HomeContainer);