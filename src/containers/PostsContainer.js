import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { withRouter } from 'react-router-dom';
import { connect } from 'react-redux';
import AppFrame from '../components/AppFrame';
import PostsList from '../components/PostsList';
import PostsActions from '../components/PostsActions';
import { fetchPosts } from '../actions/fetchPosts';
import { getPosts } from './../selectors/posts';

class PostsContainer extends Component {

    componentDidMount() {
        if (this.props.posts.length === 0) {
            this.props.fetchPosts();
        }
    }
     
    handleAddNew = () => {
        this.props.history.push('/posts/new');
    }

    renderBody = posts => (
        <div>
            <PostsList 
                posts={posts} 
                urlPath={'posts/'} >
            </PostsList>
            <PostsActions>
                <button onClick={this.handleAddNew}>New post</button>
            </PostsActions>
        </div>
    )

    render() {
        return (
            <div>
                <AppFrame header={`Titles `}
                    body={this.renderBody(this.props.posts)}></AppFrame>
            </div>
        );
    }
}

PostsContainer.propTypes = {
    fetchPosts: PropTypes.func.isRequired,
    posts: PropTypes.array.isRequired,
};

PostsContainer.defaultProps = {
    posts:  [ ]    
};

const mapStateToProps = state => ({
    posts: getPosts(state)
});

export default withRouter(connect(mapStateToProps, { fetchPosts })(PostsContainer));