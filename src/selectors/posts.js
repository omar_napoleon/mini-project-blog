import { createSelector } from 'reselect';

export const getPosts = state => state.posts;

export const getPostById = createSelector(
    (state, props) => state.posts.find( c => c.id == props.id), 
    post => post
);