import React from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';

const PostItem = ({ id, title, body, editAction, delAction, urlPath}) => {
    return (
        <div className="posts-list-item">
            <div className="field">
                <Link to={`${urlPath}${id}`}>{title}</Link>
            </div>
            <div className="field">
                <Link to={`${urlPath}${id}/edit`}>{editAction}</Link>
            </div>
            <div className="field">
                <Link to={`${urlPath}${id}/del`}>{delAction}</Link>
            </div>
        </div>
    );
};

PostItem.propTypes = {
    id: PropTypes.number.isRequired,
    title: PropTypes.string.isRequired,
    body: PropTypes.string.isRequired,
    editAction: PropTypes.string.isRequired,
    delAction: PropTypes.string.isRequired,
    urlPath: PropTypes.string.isRequired,
};

export default PostItem;