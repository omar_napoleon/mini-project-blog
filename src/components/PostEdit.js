import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { reduxForm, Field } from 'redux-form';
import { setPropsAsInitial } from '../helpers/setPropsAsInitial';
import PostsActions from './PostsActions';
import { Prompt } from 'react-router-dom';
import { POST_EDIT } from '../constants/permissions';
import { accessControl } from '../helpers/accessControl';


const validate = values => {
    const error = {};

    if (!values.body) {
        error.body = "The field is required";

    }

    if (!values.title) {
        error.title = "The field is required";
    }

    return error;
};


const toUpper = value => value && value.toUpperCase();
const toLower = value => value && value.toLowerCase();
class PostEdit extends Component {

    componentDidMount() {
        if (this.txt) {
            this.txt.focus();
        }
        this.render()

    }

    renderField = ({input, meta, type, label, name, withFocus}) => (
        <div>
            <label htmlFor={name}>{label}</label>
            <input {...input} 
                    type={!type ? "text" : type}
                    ref={withFocus && (txt => { this.txt = txt;} ) } />
            {
                meta.touched && meta.error && <span>{meta.error}</span>
            }
        </div>
    );    

    render() {
        const { handleSubmit, submitting, onBack, pristine, submitSucceeded } = this.props;
        return (
            <div>
                <form onSubmit={handleSubmit}>
                    <div>
                        <Field 
                            withFocus
                            name="title" 
                            component={this.renderField} 
                            label="Title"
                            parse={toUpper}
                            format={toLower}>
                            </Field>
                    </div>
                    <div>
                        <Field 
                            name="body" 
                            component={this.renderField} 
                            label="Body"
                            type="textarea">
                        </Field>
                    </div>
                    <PostsActions>
                        <button type="submit" disabled={pristine || submitting}>
                            Save
                        </button>
                        <button type="button" disabled={submitting} onClick={onBack}>
                            Cancel
                        </button>
                    </PostsActions>
                    <Prompt
                        when={!pristine && !submitSucceeded}
                        message="you are sure to cancel?"></Prompt>
                </form>
            </div>
        );        
    }
};

PostEdit.propTypes = {
    body: PropTypes.string,
    id: PropTypes.number,
    onBack: PropTypes.func.isRequired,
};

const PostEditForm = reduxForm(
    { 
        form: 'PostEdit',
        validate
    })(PostEdit);

export default accessControl([POST_EDIT])(setPropsAsInitial(PostEditForm)); 