import React from 'react';
import PropTypes from 'prop-types';
import PostItem from './PostItem';
import { POST_LIST } from '../constants/permissions';
import { accessControl } from '../helpers/accessControl';
import { Pagination, Pager } from 'react-bootstrap';

const PostsList = ({ posts, urlPath }) => {
    console.log("aquiiii");
    console.log(posts);
// const postss = posts.reverse();
    return (
        
        <div className="posts-list">
            {
                posts.map( c => {
                    return(
                        
                    <PostItem
                        key={`${c.id}${c.id}`}
                        id={c.id}
                        title={c.title}
                        body={c.body}
                        editAction={'Edit'}
                        delAction={'Delete'}
                        urlPath={urlPath}>
                    </PostItem>);
                    })
            }
        </div>          
    );
};

PostsList.propTypes = {
    posts: PropTypes.array.isRequired,
    urlPath: PropTypes.string.isRequired,
};

export default accessControl([POST_LIST])(PostsList);