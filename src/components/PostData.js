import React from 'react';
import PropTypes from 'prop-types';
import PostsActions from './PostsActions';
import { accessControl } from '../helpers/accessControl';
import { POST_VIEW } from '../constants/permissions';

const PostData = ( { 
        id,title, body,  onBack, isDeleteAllow, onDelete 
    }) => {
    return (
        <div>
            <div className="post-data">
                <h2>Data Post</h2>
                <div><strong>title: </strong><h4 >{title}</h4></div>
                <div><strong>Body: </strong><h5>{body}</h5></div>
            </div>
            <PostsActions>
                <button onClick={onBack}>Back</button>
                {isDeleteAllow && <button onClick={() => onDelete(id)}>Delete</button>}
            </PostsActions>
        </div>
    );
};

PostData.propTypes = {
    id: PropTypes.number.isRequired,
    body: PropTypes.string.isRequired,
    title: PropTypes.string,
    onBack: PropTypes.func.isRequired,
    isDeleteAllow: PropTypes.bool,
    onDelete: PropTypes.func,
};

export default accessControl([POST_VIEW])(PostData);