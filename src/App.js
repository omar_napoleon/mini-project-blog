import React, { Component } from 'react';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import './App.css';
import HomeContainer from './containers/HomeContainer';
import PostsContainer from './containers/PostsContainer';
import PostContainer from './containers/PostContainer';
import NewPostContainer from './containers/NewPostContainer';

class App extends Component {

  renderHome = () => <h1>Home</h1>;

  renderPostContainer = () => <h1>Post Container</h1>;

  renderPostListContainer = () => <h1>Posts List Container</h1>;

  render() {
    return (
      <Router>
        <div>
          <Route exact path="/" component={HomeContainer} />
          <Route exact path="/posts" component={PostsContainer} />
          <Switch>
            <Route path="/posts/new" component={NewPostContainer} />
            <Route path="/posts/:id" 
                    render={props => <PostContainer {...props} id={props.match.params.id} />} />
          </Switch>
        </div>
      </Router>
    );
  }
}

export default App;
