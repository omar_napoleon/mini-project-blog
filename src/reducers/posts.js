import { handleActions } from 'redux-actions';
import { FETCH_POSTS, 
    INSERT_POST, 
    UPDATE_POST, 
    DELETE_POST } from '../constants/index';

export const posts = handleActions({
    [FETCH_POSTS]: (state, action) => [ ...action.payload],
    [INSERT_POST]: (state, action) => [ ...state, action.payload ],
    [UPDATE_POST]: (state, action) => {
        const postPayload = action.payload;
        const { id } = postPayload; 
        const posts = state;
        const initialValue = [];
       
        const newPosts = posts.reduce( (acc, post) => {
            if (post.id === id) {
                return [ ...acc, postPayload];
            } else {
                return [ ...acc, post ];
            }
        }, initialValue);

        return newPosts;
    },
    [DELETE_POST]: (state, action) => state.filter(c => c.id !== action.payload)
}, []);

