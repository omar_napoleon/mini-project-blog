import { combineReducers } from 'redux';
import { posts } from './posts';
import { reducer as reduxForm } from 'redux-form';
import { POST_VIEW, POST_LIST, POST_EDIT } from './../constants/permissions';

const user = (state, action) => (
    {
        permissions: [POST_LIST, POST_VIEW, POST_EDIT]
    }
);

export default combineReducers({
    posts,
    form: reduxForm,
    user
});