
import { createAction } from 'redux-actions';
import { DELETE_POST } from '../constants/index';
import { apiDelete } from '../api';
import { urlPosts } from '../api/urls';

export const deletePost = createAction(DELETE_POST, 
    id => apiDelete(urlPosts, id)() );