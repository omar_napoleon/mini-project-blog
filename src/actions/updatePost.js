
import { createAction } from 'redux-actions';
import { UPDATE_POST } from '../constants/index';
import { apiPut } from '../api';
import { urlPosts } from '../api/urls';

export const updatePost = createAction(UPDATE_POST, 
    (id, post) => apiPut(urlPosts, id, post)() );