
import { createAction } from 'redux-actions';
import { INSERT_POST } from '../constants/index';
import { apiPost } from '../api';
import { urlPosts } from '../api/urls';

export const insertPost = createAction(INSERT_POST, 
    post => apiPost(urlPosts, post)() );