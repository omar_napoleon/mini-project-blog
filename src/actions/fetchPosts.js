import { FETCH_POSTS } from "../constants";
import { createAction } from 'redux-actions';
import { apiGet } from '../api';
import { urlPosts } from "../api/urls";

export const fetchPosts = createAction(FETCH_POSTS, apiGet(urlPosts));