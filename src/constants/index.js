export const FETCH_POSTS = 'FETCH_POSTS';
export const UPDATE_POST = 'UPDATE_POST';
export const INSERT_POST = 'INSERT_POST';
export const DELETE_POST = 'DELETE_POST';
